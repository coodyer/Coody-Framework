package org.coody.framework.logged.entity;

public class LoggedEntity {

	public LoggedEntity(String msg, String level, Throwable ex, Object[] parameter, StackTraceElement[] stack) {
		super();
		this.msg = msg;
		this.level = level;
		this.ex = ex;
		this.parameter = parameter;
		this.stack = stack;
	}

	String msg;
	String level;
	Throwable ex;
	Object[] parameter;
	StackTraceElement[] stack;

	
	
	public StackTraceElement[] getStack() {
		return stack;
	}

	public void setStack(StackTraceElement[] stack) {
		this.stack = stack;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Throwable getEx() {
		return ex;
	}

	public void setEx(Throwable ex) {
		this.ex = ex;
	}

	public Object[] getParameter() {
		return parameter;
	}

	public void setParameter(Object[] parameter) {
		this.parameter = parameter;
	}

}
