package org.coody.framework.core.loader;

import java.util.ArrayList;
import java.util.List;

import org.coody.framework.core.bean.InitBeanFace;
import org.coody.framework.core.container.BeanContainer;
import org.coody.framework.core.loader.iface.CoodyLoader;
import org.coody.framework.core.threadpool.ThreadBlockPool;
import org.coody.framework.core.util.CommonUtil;
import org.coody.framework.core.util.abnormal.PrintException;

/**
 * 
 * @author Coody
 *
 */
public class InitRunLoader implements CoodyLoader {

	@Override
	public void doLoader() throws Exception {
		List<Runnable> inits = new ArrayList<Runnable>();
		for (Object bean : BeanContainer.getBeans()) {
			if (!InitBeanFace.class.isAssignableFrom(bean.getClass())) {
				continue;
			}
			// 初始化运行
			try {
				inits.add(new Runnable() {
					@Override
					public void run() {
						try {
							((InitBeanFace) bean).init();
						} catch (Exception e) {
							PrintException.printException(e);
						}
					}
				});
			} catch (Exception e) {
				PrintException.printException(e);
			}
		}
		if (!CommonUtil.isNullOrEmpty(inits)) {
			new ThreadBlockPool().execute(inits);
		}
	}

}
