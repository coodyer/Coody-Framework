package org.coody.framework.core.util.http.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.coody.framework.core.util.GZIPUtils;

@SuppressWarnings("serial")
public class HttpEntity implements Serializable {

	private Integer code;

	private byte[] body;

	private boolean isGzip;

	private boolean chunked;

	private Map<String, String> header = new HashMap<String, String>();

	public boolean isChunked() {
		return chunked;
	}

	public void setChunked(boolean chunked) {
		this.chunked = chunked;
	}

	public boolean isGzip() {
		return isGzip;
	}

	public void setGzip(boolean isGzip) {
		this.isGzip = isGzip;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public byte[] getBody() {
		if (isGzip) {
			return GZIPUtils.uncompress(this.body);
		}
		return body;
	}

	public String getBody(String encode) throws UnsupportedEncodingException {
		return new String(this.getBody(), encode);
	}

	public void setBody(byte[] body) {
		this.body = body;
	}

	public Map<String, String> getHeader() {
		return header;
	}

	public void setHeader(Map<String, String> header) {
		this.header = header;
	}

}
