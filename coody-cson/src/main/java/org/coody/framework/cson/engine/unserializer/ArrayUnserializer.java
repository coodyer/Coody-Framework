package org.coody.framework.cson.engine.unserializer;

import java.lang.reflect.Array;
import java.util.Collection;

import org.coody.framework.cson.engine.unserializer.iface.AbstractUnserializer;
import org.coody.framework.cson.entity.ObjectWrapper;
import org.coody.framework.cson.entity.TypeEntity;
import org.coody.framework.cson.util.FieldUtil;

public class ArrayUnserializer extends AbstractUnserializer {

	CollectionUnserializer collectionInterpreter = new CollectionUnserializer();

	@SuppressWarnings("unchecked")
	@Override
	public <T> ObjectWrapper<T> doAdapter(String json, TypeEntity type, int offset) {
		ObjectWrapper<T> wrapper = collectionInterpreter.doAdapter(json, type, offset);
		if (wrapper.getObject() == null) {
			return wrapper;
		}
		wrapper.setObject((T) collectionToArray((Collection<?>) wrapper.getObject(), type.getCurrent()));
		return wrapper;
	}

	private static Object[] collectionToArray(Collection<?> collection, Class<?> type) {
		Object[] args = (Object[]) Array.newInstance(type.getComponentType(), collection.size());
		int index = 0;
		for (Object line : collection) {
			args[index] = FieldUtil.parseValue(line, type);
			index++;
		}
		return args;
	}

}
