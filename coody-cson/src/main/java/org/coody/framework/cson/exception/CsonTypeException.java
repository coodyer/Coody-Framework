package org.coody.framework.cson.exception;

@SuppressWarnings("serial")
public class CsonTypeException extends CsonException {

	public CsonTypeException(String msg) {
		super(msg);
	}
}
