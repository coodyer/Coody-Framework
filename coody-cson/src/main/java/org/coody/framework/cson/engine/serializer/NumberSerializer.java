package org.coody.framework.cson.engine.serializer;

import org.coody.framework.cson.engine.serializer.iface.AbstractSerializer;

public class NumberSerializer extends AbstractSerializer<Number> {

	@Override
	public String doAdapter(Number target) {
		return target.toString();
	}

}
