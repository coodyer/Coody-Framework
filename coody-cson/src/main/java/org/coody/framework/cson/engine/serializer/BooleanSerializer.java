package org.coody.framework.cson.engine.serializer;

import org.coody.framework.cson.engine.serializer.iface.AbstractSerializer;

public class BooleanSerializer extends AbstractSerializer<Boolean> {

	@Override
	public String doAdapter(Boolean target) {
		return target.toString();
	}

}
