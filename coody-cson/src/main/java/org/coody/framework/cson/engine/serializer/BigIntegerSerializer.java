package org.coody.framework.cson.engine.serializer;

import java.math.BigInteger;

import org.coody.framework.cson.engine.serializer.iface.AbstractSerializer;

public class BigIntegerSerializer extends AbstractSerializer<BigInteger> {

	@Override
	public String doAdapter(BigInteger target) {
		return target.toString();
	}

}
