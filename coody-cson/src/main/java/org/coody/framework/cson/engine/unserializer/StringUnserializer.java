package org.coody.framework.cson.engine.unserializer;

import org.coody.framework.cson.engine.unserializer.iface.AbstractUnserializer;
import org.coody.framework.cson.entity.ObjectWrapper;
import org.coody.framework.cson.entity.TypeEntity;
import org.coody.framework.cson.exception.CsonFormatException;

public class StringUnserializer extends AbstractUnserializer {

	@SuppressWarnings("unchecked")
	@Override
	public <T> ObjectWrapper<T> doAdapter(String json, TypeEntity type, int offset) {

		OutputSymboler output = getOutputSymboler(json, offset);
		if (output == null) {
			throw new CsonFormatException("错误的Json格式");
		}

		StringBuilder temp = new StringBuilder();

		boolean inContent = false;

		boolean isString = false;

		char lastChr = '0';

		ObjectWrapper<T> wrapper = new ObjectWrapper<T>();

		int length = 0;

		for (int i = offset + output.getLength()-1; i < json.length(); i++) {
			length++;
			char chr = json.charAt(i);
			temp.append(chr);
			if (chr == '"') {
				if (lastChr != '\\') {
					inContent = inContent ? false : true;
					isString = true;
					continue;
				}
			}
			if (!inContent) {
				// 出栈
				if (chr == output.getOutputSymbol()) {
					isString = false;
					break;
				}
			}
			lastChr = chr;
			if (!isString && chr == ' ') {
				continue;
			}
		}
		wrapper.setObject((T) temp.toString());
		wrapper.setLength(length);
		return wrapper;
	}

}
