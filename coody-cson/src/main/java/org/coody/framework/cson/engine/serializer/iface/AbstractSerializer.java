package org.coody.framework.cson.engine.serializer.iface;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.chrono.ChronoLocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.coody.framework.cson.engine.serializer.ArraySerializer;
import org.coody.framework.cson.engine.serializer.BigDecimalSerializer;
import org.coody.framework.cson.engine.serializer.BigIntegerSerializer;
import org.coody.framework.cson.engine.serializer.BooleanSerializer;
import org.coody.framework.cson.engine.serializer.CollectionSerializer;
import org.coody.framework.cson.engine.serializer.DateSerializer;
import org.coody.framework.cson.engine.serializer.EnumSerializer;
import org.coody.framework.cson.engine.serializer.IteratorSerializer;
import org.coody.framework.cson.engine.serializer.LocalDateTimeSerializer;
import org.coody.framework.cson.engine.serializer.MapSerializer;
import org.coody.framework.cson.engine.serializer.NumberSerializer;
import org.coody.framework.cson.engine.serializer.ObjectSerializer;
import org.coody.framework.cson.engine.serializer.PrimitiveSerializer;
import org.coody.framework.cson.engine.serializer.StringSerializer;
import org.coody.framework.cson.engine.serializer.ThrowableSerializer;

@SuppressWarnings({ "unchecked", "rawtypes" })
public abstract class AbstractSerializer<T> {

	private static AbstractSerializer<Object> objectSerializer = new ObjectSerializer();

	private static AbstractSerializer<Object> primitiveSerializer = new PrimitiveSerializer();

	private static final Map<Class<?>, AbstractSerializer<?>> SERIALIZER_CONTAINER = new HashMap<Class<?>, AbstractSerializer<?>>();

	private static final Map<Class<?>, AbstractSerializer<?>> SOURCE_CONTAINER = new LinkedHashMap<Class<?>, AbstractSerializer<?>>();

	static {
		addSerializer(Number.class, new NumberSerializer());
		addSerializer(String.class, new StringSerializer());
		addSerializer(Date.class, new DateSerializer());
		addSerializer(ChronoLocalDateTime.class, new LocalDateTimeSerializer());
		addSerializer(Throwable.class, new ThrowableSerializer());
		addSerializer(Boolean.class, new BooleanSerializer());
		addSerializer(Collection.class, new CollectionSerializer());
		addSerializer(Iterator.class, new IteratorSerializer());
		addSerializer(Map.class, new MapSerializer());
		addSerializer(Enum.class, new EnumSerializer());
		addSerializer(BigDecimal.class, new BigDecimalSerializer());
		addSerializer(BigInteger.class, new BigIntegerSerializer());
		addSerializer(Object[].class, new ArraySerializer());

	}

	public static String serialize(Object target) {
		if (target == null) {
			return null;
		}
		AbstractSerializer serializer = getSerializer(target.getClass());
		return serializer.doAdapter(target);

	}

	public static synchronized void addSerializer(Class<?> clazz, AbstractSerializer<?> serializer) {
		SOURCE_CONTAINER.put(clazz, serializer);
		SERIALIZER_CONTAINER.put(clazz, serializer);
	}

	private static AbstractSerializer<?> getSerializer(Class<?> clazz) {
		AbstractSerializer<?> serializer = SERIALIZER_CONTAINER.get(clazz);
		if (serializer != null) {
			return serializer;
		}
		try {
			if (clazz.isPrimitive()) {
				serializer = primitiveSerializer;
				return serializer;
			}
			for (Class<?> key : SOURCE_CONTAINER.keySet()) {
				if (key.isAssignableFrom(clazz)) {
					serializer = SOURCE_CONTAINER.get(key);
					return serializer;
				}
			}
			serializer = objectSerializer;
			return serializer;
		} finally {
			SERIALIZER_CONTAINER.put(clazz, serializer);
		}
	}

	protected abstract String doAdapter(T target);
}
