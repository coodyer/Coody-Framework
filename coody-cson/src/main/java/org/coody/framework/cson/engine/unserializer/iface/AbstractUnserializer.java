package org.coody.framework.cson.engine.unserializer.iface;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.coody.framework.cson.adapter.TypeAdapter;
import org.coody.framework.cson.convert.ValueConvert;
import org.coody.framework.cson.engine.unserializer.ArrayUnserializer;
import org.coody.framework.cson.engine.unserializer.CollectionUnserializer;
import org.coody.framework.cson.engine.unserializer.MapUnserializer;
import org.coody.framework.cson.engine.unserializer.ObjectUnserializer;
import org.coody.framework.cson.engine.unserializer.StringUnserializer;
import org.coody.framework.cson.entity.JsonFieldEntity;
import org.coody.framework.cson.entity.ObjectWrapper;
import org.coody.framework.cson.entity.TypeEntity;
import org.coody.framework.cson.exception.CsonException;
import org.coody.framework.cson.exception.CsonFormatException;
import org.coody.framework.cson.util.FieldUtil;
import org.coody.framework.cson.util.TypeUtil;

@SuppressWarnings({ "rawtypes", "unchecked" })
public abstract class AbstractUnserializer {

	protected static AbstractUnserializer arrayUnserializer = new ArrayUnserializer();

	protected static AbstractUnserializer collectionUnserializer = new CollectionUnserializer();

	protected static AbstractUnserializer mapUnserializer = new MapUnserializer();

	protected static AbstractUnserializer objectUnserializer = new ObjectUnserializer();

	protected static StringUnserializer stringUnserializer = new StringUnserializer();

	public static <T> ObjectWrapper<T> unserializer(String json, Class<T> clazz) {
		return unserializer(json, TypeUtil.getTypeEntityByType((Type) clazz));
	}

	public static <T> ObjectWrapper<T> unserializer(String json, TypeAdapter<T> adapter) {
		return unserializer(json, TypeUtil.getTypeEntityByType(adapter.getType()));
	}

	public static <T> ObjectWrapper unserializer(String json, TypeEntity type) {
		return unserializer(json, type, 0);
	}

	public static <T> ObjectWrapper unserializer(String json, TypeEntity type, int offset) {
		json = json.trim();

		offset = offset - 1;
		OutputSymboler output = getOutputSymboler(json, offset);
		if (output == null) {
			throw new CsonFormatException("错误的Json格式");
		}
		offset += output.getLength() - 1;
		Character outputCharacter = output.getOutputSymbol();
		if (outputCharacter == ']') {
			if (type == null) {
				type = TypeUtil.getTypeEntityByType(new TypeAdapter<List<Object>>() {
				}.getType());
			}
			if (type.getCurrent().isArray()) {
				return arrayUnserializer.doAdapter(json, type, offset);
			}
			return collectionUnserializer.doAdapter(json, type, offset);
		}
		if (outputCharacter == '}') {
			if (type == null) {
				type = TypeUtil.getTypeEntityByType(new TypeAdapter<Map<Object, Object>>() {
				}.getType());
			}
			if (Map.class.isAssignableFrom(type.getCurrent())) {
				if (type.getActuals() == null || type.getActuals().size() < 2) {
					type = TypeUtil.getTypeEntityByType(new TypeAdapter<Map<Object, Object>>() {
					}.getType());
				}
				return mapUnserializer.doAdapter(json, type, offset);
			}
			if (type.getCurrent() == Object.class) {
				type = TypeUtil.getTypeEntityByType(new TypeAdapter<Map<Object, Object>>() {
				}.getType());
				return mapUnserializer.doAdapter(json, type, offset);
			}
			if (type.getCurrent() == String.class) {
				return stringUnserializer.doAdapter(json, type, offset);
			}
			return objectUnserializer.doAdapter(json, type, offset);
		}
		return null;
	}



	protected static void setFieldValue(Object object, String field, Object value) {
		try {
			if (value == null) {
				return;
			}
			JsonFieldEntity jsonFieldEntity = FieldUtil.getDeclaredField(object.getClass(), field);
			setFieldValue(object, jsonFieldEntity, value);
		} catch (Exception e) {
			throw new CsonException("字段赋值失败>>" + field);
		}
	}

	protected static void setFieldValue(Object object, JsonFieldEntity field, Object value) {
		try {
			if (field == null) {
				return;
			}
			if (value == null) {
				return;
			}
			if (field.getIsIgonre()) {
				return;
			}
			field.getField().set(object, ValueConvert.convert(value, field.getField().getType()));
		} catch (Exception e) {
			throw new CsonException("字段赋值失败>>" + field.getField().getName(), e);
		}
	}

	protected static OutputSymboler getOutputSymboler(String json, int offset) {
		int length = 0;
		for (int i = offset; i < json.length(); i++) {
			length++;
			if (i < 0) {
				continue;
			}
			char chr = json.charAt(i);
			if (chr == '{') {
				return new OutputSymboler('}', length);
			}
			if (chr == '[') {
				return new OutputSymboler(']', length);
			}
		}
		return null;
	}

	protected static class OutputSymboler {

		private Character outputSymbol;

		private int length;

		public OutputSymboler(Character outputSymbol, int length) {
			super();
			this.outputSymbol = outputSymbol;
			this.length = length;
		}

		public Character getOutputSymbol() {
			return outputSymbol;
		}

		public void setOutputSymbol(Character outputSymbol) {
			this.outputSymbol = outputSymbol;
		}

		public int getLength() {
			return length;
		}

		public void setLength(int length) {
			this.length = length;
		}
	}
	
	/**
	 * 
	 * @param <T>
	 * @param json
	 * @param type
	 * @param outputCharacter
	 * @param offset
	 * @return
	 */
	public abstract <T> ObjectWrapper<T> doAdapter(String json, TypeEntity type, int offset);
}
