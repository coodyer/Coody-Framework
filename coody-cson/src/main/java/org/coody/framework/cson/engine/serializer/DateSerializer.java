package org.coody.framework.cson.engine.serializer;

import java.util.Date;

import org.coody.framework.cson.engine.serializer.iface.AbstractSerializer;

public class DateSerializer extends AbstractSerializer<Date> {

	@Override
	public String doAdapter(Date target) {
		return ((Long) target.getTime()).toString();
	}

}