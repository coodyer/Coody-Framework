package org.coody.framework.cson.engine.serializer;

import java.math.BigDecimal;

import org.coody.framework.cson.engine.serializer.iface.AbstractSerializer;

public class BigDecimalSerializer extends AbstractSerializer<BigDecimal> {

	@Override
	public String doAdapter(BigDecimal target) {
		return target.toPlainString();
	}

}
