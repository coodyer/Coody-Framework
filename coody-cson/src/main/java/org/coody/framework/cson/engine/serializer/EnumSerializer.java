package org.coody.framework.cson.engine.serializer;

import org.coody.framework.cson.engine.serializer.iface.AbstractSerializer;

public class EnumSerializer extends AbstractSerializer<Enum<?>> {

	@Override
	public String doAdapter(Enum<?> target) {
		return "\"" + target.name() + "\"";
	}

}
