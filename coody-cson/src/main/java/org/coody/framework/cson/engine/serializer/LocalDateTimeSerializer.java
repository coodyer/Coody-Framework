package org.coody.framework.cson.engine.serializer;

import java.time.ZoneOffset;
import java.time.chrono.ChronoLocalDateTime;

import org.coody.framework.cson.engine.serializer.iface.AbstractSerializer;

public class LocalDateTimeSerializer extends AbstractSerializer<ChronoLocalDateTime<?>> {

	@Override
	public String doAdapter(ChronoLocalDateTime<?> target) {
		return target.atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli() + "";
	}

}