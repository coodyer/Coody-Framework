package org.coody.framework.cson.engine.serializer;

import org.coody.framework.cson.engine.serializer.iface.AbstractSerializer;

public class StringSerializer extends AbstractSerializer<String> {

	@Override
	public String doAdapter(String target) {
		return "\"" + target.replace("\r", "\\r").replace("\n", "\\n").replace("\t", "\\t").replace("\0", "\\0").replace("\"", "\\\"") + "\"";
	}

}
