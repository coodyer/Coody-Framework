package org.coody.framework.cson.engine.serializer;

import org.coody.framework.cson.engine.serializer.iface.AbstractSerializer;

public class PrimitiveSerializer extends AbstractSerializer<Object> {

	@Override
	public String doAdapter(Object target) {
		return target.toString();
	}

}
