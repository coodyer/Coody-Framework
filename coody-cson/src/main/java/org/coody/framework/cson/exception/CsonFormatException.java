package org.coody.framework.cson.exception;

@SuppressWarnings("serial")
public class CsonFormatException extends CsonException {

	public CsonFormatException(String msg) {
		super(msg);
	}
}
